# Use the file name mbox-short.txt as the file name
fname = input('Enter the file name: ')
try:
    fhand = open(fname)
    count = 0
    total = 0
    answer = 0
except:
    print('File cannot be opened:',fname)
    quit()

for line in fhand:
    line = line.rstrip()
    if not line.startswith("X-DSPAM-Confidence:"): continue
    count += 1
    startnum = line.find('0')
    num = float(line[startnum:])
    total = num + total
answer = total / count
        
        
print("Average spam confidence:",answer)
